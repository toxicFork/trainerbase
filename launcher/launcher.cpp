#include "launcher.h"
//#include "hooky.h"

#include <QtGui>
#include "console.h"
//#include <iostream>
//#include <testdll.h>
#include <windows.h>
#include <sstream>
#include <boost/filesystem.hpp>

#include <iostream>

namespace FS = boost::filesystem;

template<typename T> T getProc(FARPROC fp)
{
	return reinterpret_cast<T>(fp);
}

Launcher::Launcher()
{
	//Hook::setup();
	//std::vector<HookPatch> patches = Hook::getPatches();
	//setup();
//	Santosi hook;
//	initHook();

	QWidget *mainWidget = new QWidget;

	QVBoxLayout *layout = new QVBoxLayout;

	console = new Console;
	layout->addWidget(console);

	originalcoutBuf = std::cout.rdbuf(console);

	FS::path p (".");
	if (FS::is_directory("."))      // is p a directory?
	{
		std::cout << p << " is a directory containing:\n";

		typedef std::vector<boost::filesystem::path> vec;             // store paths,
		vec v;                                // so we can sort them later

		copy(FS::directory_iterator(p), FS::directory_iterator(), back_inserter(v));
		for (vec::const_iterator it (v.begin()); it != v.end(); ++it)
		{
			if((FS::extension(*it)==".dll"))
			{
				std::cout<<"Loading library: "<<*it<<"!"<<std::endl;
				HMODULE dllModule = LoadLibrary((FS::absolute(*it)).string().c_str());
				if(dllModule)
				{
					std::cout<<"Library loaded: "<<*it<<"!"<<std::endl;
					if(FARPROC initializeHook = GetProcAddress(dllModule,"initializeHook"))
					{
						if(initializeHook()!=0)
						{
							std::cout<<"Hook initialization failed!"<<std::endl;
							std::cout<<"Freeing library: "<<*it<<std::endl;
							FreeLibrary(dllModule);
							std::cout<<"Free complete:"<<*it<<"!"<<std::endl;
						}
						else
						{
							//std::cout<<GetCurrentProcessId()<<std::endl;
							char gameName[MAX_PATH] = "";
							getProc<void (*)(char*)>(GetProcAddress(dllModule,"getGameName"))(gameName);
							std::cout<<"Game name: "<<gameName<<std::endl;
							GetProcAddress(dllModule,"start")();
							modules.push_back(dllModule);
						}
					}
					else
					{
						std::cout<<"initializeHook not found!"<<std::endl;
						std::cout<<"Freeing library: "<<*it<<std::endl;
						FreeLibrary(dllModule);
						std::cout<<"Free complete:"<<*it<<"!"<<std::endl;
					}
				}
				else
				{
					std::cout<<"Failed to load hook!"<<std::endl;
				}
				//std::cout << "   " << FS::absolute(*it)<< strlen((FS::absolute(*it)).string().c_str())<< (FS::absolute(*it)).string().c_str() << '\n';
			}
		}
		//std::copy(boost::filesystem::directory_iterator(p), boost::filesystem::directory_iterator(), // directory_iterator::value_type
		//	std::ostream_iterator<boost::filesystem::directory_entry>(std::cout, "\n")); // is directory_entry, which is
		// converted to a path by the
		// path stream inserter
	}


	//hook = new Hooky("JustCause2.exe",console);

	//QPushButton *ejectButton = new QPushButton("Eject");
	//connect(ejectButton,SIGNAL(clicked()),this,SLOT(eject()));
	//layout->addWidget(ejectButton);

	mainWidget->setLayout(layout);
	setCentralWidget(mainWidget);
}

Launcher::~Launcher()
{
	std::cout.rdbuf(originalcoutBuf);

	for(std::vector<HMODULE>::iterator i = modules.begin();i!=modules.end(); ++i)
	{
		GetProcAddress(*i,"stop")();
		FreeLibrary(*i);
	}
	//delete hook;
}

//void Launcher::eject()
//{
	/*if(hook)
	{
		delete hook;
		hook = NULL;
	}
	else
		hook = new Hooky("JustCause2.exe",console);*/
//}
