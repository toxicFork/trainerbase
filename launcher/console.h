#pragma once

#include <streambuf>
#include <QWidget>
#include <vector>
class QTextEdit;

class Console : public QWidget, public std::streambuf
{
	Q_OBJECT

public:
	Console();
public slots:
	void addToLog(const QString &str);
private:
	QTextEdit *textEdit;
	std::vector<char> buffer_;

	int overflow(int ch);
	int sync();
	void flush();
	
	// copy ctor and assignment not implemented;
	// copying not allowed
	Console(const Console &);
	Console &operator= (const Console &);
};