#include <QApplication>
#include "launcher.h"
#include <Windows.h>

#pragma data_seg(".shared")
HWND hGlobal = {0};
#pragma data_seg()
#pragma comment (linker,"/section:.shared,RWS")

int main(int argc, char** argv)
{
	bool AlreadyRunning;

	HANDLE hMutexOneInstance = CreateMutex( NULL, TRUE,"RUNhjZ7r5zFJ9WLAPTUW");

	AlreadyRunning = (GetLastError() == ERROR_ALREADY_EXISTS);

	if (hMutexOneInstance != NULL) 
	{
		ReleaseMutex(hMutexOneInstance);
	}

	if ( AlreadyRunning )
	{ /* kill this */
		HWND hOther = hGlobal;

		if (hOther != NULL)
		{ /* pop up */
			SetForegroundWindow(hOther);

			if (IsIconic(hOther))
			{ /* restore */
				ShowWindow(hOther, SW_RESTORE);
			} /* restore */
		} /* pop up */

		return -1; // terminates the creation
	}

	QApplication app(argc, argv);
	Launcher window;
	window.show();
	hGlobal = window.winId();
	return app.exec();
}