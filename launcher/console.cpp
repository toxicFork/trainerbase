#include "console.h"

#include <QtGui>
#include <sstream>
#include <cassert>
#include <Windows.h>
#include <QString>

CRITICAL_SECTION traceSection;

Console::Console()
	:buffer_(1)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	textEdit = new QTextEdit();
	textEdit->setLineWrapMode(QTextEdit::NoWrap);
	textEdit->setReadOnly(true);
	layout->addWidget(textEdit);

	char *base = &buffer_.front();
	setp(base, base+buffer_.size()-1);

	InitializeCriticalSection(&traceSection);
}

int Console::overflow( int ch )
{
	if (ch != traits_type::eof())
	{
		EnterCriticalSection(&traceSection);
		assert(std::less_equal<char *>()(pptr(), epptr()));
		*pptr() = ch;
		pbump(1);

		flush();

		LeaveCriticalSection(&traceSection);
		return ch;
	}
	return traits_type::eof();
}

int Console::sync()
{
	EnterCriticalSection(&traceSection);
	flush();
	LeaveCriticalSection(&traceSection);
	return 0;
}

void Console::addToLog(const QString &str)
{
	textEdit->moveCursor(QTextCursor::End);
	textEdit->insertPlainText(str);
	textEdit->repaint();
}

void Console::flush()
{
	std::ptrdiff_t n = pptr() - pbase();
	pbump(-n);

	std::ostringstream sink;
	sink.write(pbase(),n);

	// instead of: someOtherQObject->updateProgress(percent);
	QMetaObject::invokeMethod(this,"addToLog", Qt::QueuedConnection, Q_ARG(const QString&, sink.str().c_str()));
}

