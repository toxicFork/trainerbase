#pragma once

#include <QMainWindow>
#include <streambuf>
#include <vector>
#include <Windows.h>

//class Hooky;
class Console;

class Launcher : public QMainWindow
{
	Q_OBJECT
public:
	Launcher();
	~Launcher();
//private slots:
	//void eject();

private:
	//Hooky *hook;
	Console *console;
	std::streambuf *originalcoutBuf;
	std::vector<HMODULE> modules;
};