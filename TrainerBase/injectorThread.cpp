#include "injectorThread.h"
#include "trainerBase.h"
#include "hook.h"
#include <iostream>

InjectorThread::InjectorThread(Trainer* _parent, DWORD _pid)
	:parent(_parent),
	gamePid(_pid)
{
	injectSelf();
	thread = new boost::thread(&InjectorThread::run,this);
}

inline void logMessage(const char* os)
{
	std::cout<<os<<std::endl;
}

InjectorThread::~InjectorThread()
{
	LOG("CLEANING UP INJECTOR THREAD!");
	ejectSelf();

	if(thread)
	{
		LOG("INJECTOR THREAD EXISTS!");
		if(running)
		{
			LOG("INJECTOR THREAD RUNNING!");
			running = false;
			thread->join();
		}
		else
		{
			LOG("BUT IT'S NOT RUNNING!");
		}

		delete thread;
		thread = NULL;

		gamePid = NULL;
	}
}


void InjectorThread::run()
{
	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS,NULL,gamePid);
	LOG("started injector thread!");

	while(true)
	{
		if(!running)
		{
			LOG("INJECTOR THREAD STOPPED RUNNING!");
			break;
		}

		if(WaitForSingleObject(hProc,0)==WAIT_OBJECT_0)
		{
			LOG("PROCESS EXITED!");
			injectedModule = NULL;
			parent->processExited(gamePid);
			gamePid = NULL;
			break;
		}

		Sleep(20);
	}
	CloseHandle(hProc);
	running = false;
}

void InjectorThread::injectSelf()
{
	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS,NULL,gamePid);

	TCHAR szFileName[MAX_PATH];
	GetModuleFileName(parent->getModule(), szFileName, MAX_PATH);

	std::string dllPath(szFileName);
	int len = dllPath.length() + 2;
	void* pRemoteString = VirtualAllocEx(hProc, 0, len * sizeof(char), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(hProc, pRemoteString, (void*)dllPath.c_str(), len * sizeof(char), 0);
	FARPROC pLoadLibrary = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	HANDLE hThread = CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)pLoadLibrary, pRemoteString, 0, 0);
	if(hThread)
	{
		WaitForSingleObject(hThread,INFINITE);
		GetExitCodeThread(hThread, (DWORD*)&injectedModule);
		LOG("SUCCESSFULLY INJECTED INTO PROCESS!");

		SuspendThread(hProc);
		Sleep(5000);
		ResumeThread(hProc);
		CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(injectedModule,"justInjected"), NULL, 0, 0);
		hThread = NULL;
	}
	else
	{
		LOG("COULDN'T CREATE INJECTOR THREAD!");
		assert(false);
	}
	VirtualFreeEx(hProc, pRemoteString, len * sizeof(wchar_t), MEM_FREE);
	CloseHandle(hProc);

}

void InjectorThread::ejectSelf()
{
	if(injectedModule&&gamePid)
	{
		HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS,NULL,gamePid);

		WaitForSingleObject(CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(injectedModule,"justEjected"), NULL, 0, 0),INFINITE);

		FARPROC pFreeLibrary = GetProcAddress(GetModuleHandle("kernel32.dll"), "FreeLibrary");
		HANDLE hThread = CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)pFreeLibrary, (void*)injectedModule, 0, 0);
		if(hThread)
		{
			WaitForSingleObject(hThread,INFINITE);
			LOG("SUCCESSFULLY EJECTED FROM PROCESS!");
			hThread = NULL;
			injectedModule = NULL;
		}
		else
		{
			LOG("COULDN'T CREATE EJECTOR THREAD!");
			assert(false);
		}
		CloseHandle(hProc);
	}
	//else
	//{
		//LOG("NOT INJECTED INTO THE GAME");
//		assert(false);
	//}
}

/*
void Hook::insideProcess(DWORD gamePID)
{
	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS,NULL,gamePID);

	//TODO: APPLY PATCHES
	if(replaceMemcpy)
	{
	}

	while(true)
	{
		if(!running)
		{
			//TODO: REMOVE PATCHES
			break;
		}
	}

	CloseHandle(hProc);
	if(replaceMemcpy)
	{

	}
}
}*/