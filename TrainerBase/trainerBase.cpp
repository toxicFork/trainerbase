#include "trainerBase.h"
#include <Windows.h>
#include <sstream>
#include <iostream>
#include <Psapi.h>
#include <vector>
#include <boost/thread.hpp>
#include "injectorThread.h"
#include "peHeader.h"
#include <Psapi.h>
#include "hook.h"
#include <detours.h>
#include <cassert>

//#define MAGIC "U6vdXTUvr2p9vEQhmbo"

HMODULE module = NULL;
DWORD pid = NULL;
bool insideLauncher = false;
Trainer *singleton = NULL;

DWORD imageBase=NULL, textOffset=NULL, textSize=NULL;

HANDLE fileHandle=NULL, mappingHandle=NULL;
PVOID viewOfFile=NULL;

void open_file_mapping()
{
	//std::array<wchar_t, MAX_PATH> file_name;
	//GetModuleFileNameW( nullptr, &file_name[0], file_name.size() );
	TCHAR szFileName[MAX_PATH];
	GetModuleFileName(NULL, szFileName, MAX_PATH);
	//MessageBoxA(NULL,szFileName,"a",NULL);

	fileHandle = CreateFileA( szFileName, GENERIC_READ, FILE_SHARE_READ,
		nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr );

	if ( !fileHandle )
		throw std::runtime_error( "Failed to open game exe!" );

	DWORD file_size(0);
	GetFileSize( fileHandle, &file_size );

	mappingHandle = CreateFileMapping( fileHandle, nullptr, 
		PAGE_READONLY, file_size, 0, nullptr );

	//MessageBoxA(NULL,"b","",NULL);
	if ( !mappingHandle )
		throw std::runtime_error( "Failed to create file mapping" );
	//MessageBoxA(NULL,"c","",NULL);

	viewOfFile = MapViewOfFile( mappingHandle, FILE_MAP_READ, 0, 0, 0 );
	if ( !viewOfFile )
		throw std::runtime_error( "Failed to map view of file" );
	
} 

#define hook(retval,prefix,name,...)  retval ( prefix *orig_##name ) (__VA_ARGS__) = NULL;\
	retval hk_##name (__VA_ARGS__)

hook(void*,__cdecl,memcpy,void* dst,const void* src,size_t size)
{	
	void* ret = nullptr;
	// .text section base address
	static const DWORD text_start = imageBase+textOffset;
	// .text section end address
	static const DWORD text_end = text_start + textSize;

	// Offset into file where .text section starts
	static const DWORD file_text_start = 0x400;

	DWORD src_addr = reinterpret_cast<DWORD>(src);
	BYTE* view_of_file = static_cast<BYTE*>(
		static_cast<PVOID>(viewOfFile));

	// If the source address is within the .text section, return data
	// read from the file mapping instead, so that the hashing routine
	// does not see any modifications.
	if ( src_addr >= text_start && src_addr <= text_end )
	{
		ret = orig_memcpy(dst, 
			view_of_file + file_text_start + (src_addr - text_start), size);
	} else {
		ret = orig_memcpy(dst, src, size);
	}

	return ret;
};

hook(void,,DbgBreakPoint,void)
{
	return;
}

Trainer::Trainer(std::string _name, std::string _exeName)
	:name(_name),
	exeName(_exeName),
	mainThread(NULL),
	running(false),
	isInside(false)
{
	memcpyHook = new Hook("msvcr80.dll","memcpy",hk_memcpy,&(PVOID&)orig_memcpy);
	//debugBPHook = new Hook("NTDLL.DLL","DbgBreakPoint",hk_DbgBreakPoint,&(PVOID&)orig_DbgBreakPoint);
	if(singleton==NULL)
	{
		singleton = this;
	}
	else
	{
		MessageBox(NULL,"WE ALREADY HAVE A HOOK!",NULL,NULL);
		assert(false);
	}
}

Trainer::~Trainer()
{
	if(mainThread){
		if(running)
		{
			running = false;
			LOG("shutting down...");
			mainThread->join();
		}
		delete mainThread;
		mainThread = NULL;
	}
}


Patch & Trainer::addPatch( DWORD address, bool enabled, const std::string &name )
{
	patches.push_back(Patch(address,enabled,name));
	return patches[patches.size()-1];
}

void Trainer::addHook( const std::string &moduleName, const std::string &procName, PVOID hk, PVOID *orig )
{
	hooks.push_back(new Hook(moduleName,procName,hk,orig));
}

void Trainer::addHook( DWORD address, PVOID hk, PVOID *orig )
{
	hooks.push_back(new Hook(address,hk,orig));
}

BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpvReserved)
{
	//std::cout<<"DllMain called with: "<<hModule<<","<<dwReason<<","<<lpvReserved<<std::endl;
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:
		module = hModule;
		std::cout<<(module)<<" attached"<<std::endl;
		pid = GetCurrentProcessId();
		break;
	case DLL_PROCESS_DETACH:
		std::cout<<(module)<<" detached"<<std::endl;
		if(singleton)
			delete singleton;
		singleton = NULL;
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
}
//


std::vector<DWORD> findGameProcesses(const std::string &exeName, processMap procs)
{
	std::vector<DWORD> processes;
	unsigned long aProcesses[1024], cbNeeded, cProcesses;
	if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return processes;

	cProcesses = cbNeeded / sizeof(unsigned long);
	for(unsigned int i = 0; i < cProcesses; i++)
	{
		if(aProcesses[i] == 0||(procs.find(aProcesses[i])!=procs.end()))
			continue;

		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, aProcesses[i]);
		char buffer[MAX_PATH];
		GetModuleBaseName(hProcess, 0, buffer, MAX_PATH);
		CloseHandle(hProcess);
		//LOG(buffer);
		if(exeName == std::string(buffer))
			processes.push_back(aProcesses[i]);
	}
	return processes;
}

void Trainer::run()
{
	LOG("started!");
	running = true;
	while(true)
	{
		if(!running)
		{
			LOG("cleaning hook up");
			for(processMap::iterator i=processes.begin();i!=processes.end();++i)
			{
				if(i->second)
					delete i->second;
				i->second = NULL;
				//if(i->second)
				//{
					//i->second->join();
				//}
			}
			break;
		}

		for(std::vector<DWORD>::iterator i=deletionQueue.begin();i!=deletionQueue.end();++i)
		{
			DWORD pid = *i;
			delete processes[pid];
			processes[pid] = NULL;
			processes.erase(processes.find(pid));
		}
		deletionQueue.clear();

		std::vector<DWORD> procs = findGameProcesses(exeName,processes);
		for(std::vector<DWORD>::iterator i=procs.begin();i!=procs.end();++i)
		{
			
			processes[*i] = new InjectorThread(this,*i);
		}

		Sleep(100);
	}
	LOG("stopped!");
	running = false;
}

void Trainer::start()
{
	LOG("launching thread!");
	mainThread = new boost::thread(&Trainer::run,this);
}

void Trainer::processExited( DWORD pid )
{
	deletionQueue.push_back(pid);
}

HMODULE Trainer::getModule()
{
	return module;
}

void Trainer::justInjected()
{
	isInside = true;
	if(replaceMemcpy)
	{
		if(imageBase==NULL){
			char fileName[MAX_PATH];
			GetModuleFileName(NULL,fileName,MAX_PATH);
			PEHeader::getTextSectionInfo(fileName,imageBase,textOffset,textSize);
		}
		//std::ostringstream os;
		//os<<imageBase<<"\t"<<textOffset<<"\t"<<textSize;

		//LOG("MEMCPY HOOK!?");
		//MessageBox(NULL,os.str().c_str(),NULL,NULL);
		open_file_mapping();
		if(memcpyHook->applyHook())
		{
			//LOG("FAILED MEMCPY HOOK!");
			memcpyHook->setModuleName("MSVCRT.DLL");
			if(memcpyHook->applyHook())
			{
				LOG("FAILED MEMCPY HOOK! AGAIN!");
			}
			//LOG(" MEMCPY HOOK SUCCESS!");
		}
		//if(debugBPHook->applyHook())
		//{
		//	LOG("FAILED DBG HOOK!");
		//}
	}
	for(unsigned int i=0;i<patches.size();++i)
	{
		if(patches[i].isEnabled())
		{
			patches[i].apply();
		}
	}
	for(unsigned int i=0;i<hooks.size();++i)
	{
		if(hooks[i]->applyHook())
		{
			LOG("FAILED TO APPLY HOOK: "<<i);
		}
	}
}

void Trainer::justEjected()
{
	if(replaceMemcpy)
	{
		memcpyHook->remove();
	}
	for(unsigned int i=0;i<hooks.size();++i)
	{
		hooks[i]->remove();//if(hooks[i]->remove())
		//{
		//	LOG("FAILED TO REMOVE HOOK: "<<i);
		//}
	}
	//debugBPHook->remove();
	for(unsigned int i=0;i<patches.size();++i)
	{
		if(patches[i].isEnabled())
		{
			patches[i].revert();
		}
	}
}

void Trainer::logMessage( const char *os )
{
	if(isInside)
	{
		MessageBox(NULL,os,NULL,NULL);
	}
	else
	{
		std::cout<<os<<std::endl;
	}
}


HOOKAPI DWORD justInjected(LPVOID)
{
	singleton->justInjected();
//	MessageBox(NULL,"I'M IN!",NULL,NULL);
	return 0;
}

HOOKAPI DWORD justEjected(LPVOID)
{
	singleton->justEjected();
//	MessageBox(NULL,"I'M OUT!",NULL,NULL);
	return 0;
}

inline void logMessage(const char* os)
{
	std::cout<<os<<std::endl;
}

HOOKAPI void start(void)
{
	LOG("starting!");
	singleton->start();
}

HOOKAPI void stop(void)
{
	LOG("stopping!");
	delete singleton;
	singleton = NULL;
}

HOOKAPI int initializeHook(void)
{
	insideLauncher = true;

	//if(FARPROC fp = GetProcAddress(module,"hook"))
	{
		if(singleton==NULL)
		{
			LOG("Hook is null!");
			return 1;
		}
		
		//LOG("Hook is not null!");


		return 0;
		//hook = reinterpret_cast<Hook*>(fp);

		//char *magic = NULL;
		//magic = reinterpret_cast<char*>(fp);

		/*bool same = true;
		for(unsigned int i=0;i<strlen(MAGIC);i++)
		{
			if(MAGIC[i]!=(hook->magic)[i])
			{
				same = false;
				break;
			}
		}

		if(same)
		{
			LOG("Magic found!");

			return 0;
		}
		else
		{
			LOG("Magic not found!");
			return 1;
		}*/
	}
	//else
	{
	//	LOG("hook not found!");
	//	return 2;
	}
}

HOOKAPI void getGameName(DWORD address)
{
	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS,FALSE,pid);
	std::string name = singleton->getName();
	WriteProcessMemory(hProc,(LPVOID)(address),name.c_str(),name.length()+1,NULL);
	CloseHandle(hProc);
}

void startHook()
{

}
