#pragma once
#include <string>
#include <Windows.h>

class Hook
{
public:
	Hook(const std::string &procName, PVOID hk, PVOID *orig);
	Hook(const std::string &moduleName, const std::string &procName, PVOID hk, PVOID *orig);
	Hook( DWORD address, PVOID _hk, PVOID *_orig );
	long applyHook();
	void remove();
	void setModuleName( const std::string &param );
	std::string moduleName;
	const std::string funcName;
	PVOID hookFunc;
	PVOID *origFunc;
	bool isInternal;
};