#pragma once

#include <Windows.h>
#include <boost/thread.hpp>

class Trainer;

class InjectorThread
{
public:
	InjectorThread(Trainer* parent, DWORD pid);
	~InjectorThread();
private:
	void run();
	void waitForClose();
	void injectSelf();
	void ejectSelf();
	Trainer* parent;
	DWORD gamePid;
	bool running;
	boost::thread *thread;
	boost::thread *waitThread;
	HMODULE injectedModule;
};