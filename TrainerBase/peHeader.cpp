#include "peHeader.h"

void PEHeader::getTextSectionInfo( LPSTR filePath, DWORD &imageBase, DWORD &start, DWORD &size )
{
	imageBase = NULL;
	start = NULL;
	size = NULL;

	HANDLE fileHandle = CreateFile( filePath, GENERIC_READ, FILE_SHARE_READ,	nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr );
	if(!fileHandle)
		throw std::runtime_error("Couldn't open file!");

	DWORD file_size(0);
	GetFileSize( fileHandle, &file_size );
	HANDLE mappingHandle = CreateFileMapping( fileHandle, nullptr, PAGE_READONLY, file_size, 0, nullptr );
	if(!mappingHandle)
		throw std::runtime_error("Couldn't create file mapping!");

	LPVOID viewOfFile = MapViewOfFile( mappingHandle, FILE_MAP_READ, 0, 0, 0 );
	const char *fileBytes = (const char*)viewOfFile;

	PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER) fileBytes;
	fileBytes += dosHeader->e_lfanew;
	fileBytes += 
		4;
	PIMAGE_FILE_HEADER fileHeader = (PIMAGE_FILE_HEADER) fileBytes;
	fileBytes += sizeof(IMAGE_FILE_HEADER)/sizeof(char);

	PIMAGE_OPTIONAL_HEADER optionalHeader = (PIMAGE_OPTIONAL_HEADER) fileBytes;
	fileBytes += sizeof(IMAGE_OPTIONAL_HEADER)/sizeof(char);

	PIMAGE_SECTION_HEADER sections = (PIMAGE_SECTION_HEADER) fileBytes;

	for(unsigned int i=0;i<fileHeader->NumberOfSections;i++)
	{
		if(std::string((char*)sections[i].Name)==".text")
		{
			start = sections[i].VirtualAddress;
			size = sections[i].SizeOfRawData;
			imageBase = optionalHeader->ImageBase;
			break;
		}
	}

	CloseHandle(mappingHandle);
	CloseHandle(fileHandle);
}

