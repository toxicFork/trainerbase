#include "patch.h"
#include <assert.h>
#include <sstream>

Patch& Patch::operator<<( unsigned char c )
{
	bytes.push_back(c);
	return *this;
}

std::vector<unsigned char> readString(const std::string &str)
{
	std::vector<unsigned char> bytes;
	unsigned int len = str.length();

	for(unsigned int i=0;i<len/2;i++)
	{
		const std::string cur = str.substr(i*2,2);

		unsigned int x = 0;
		std::stringstream ss;
		ss << std::hex << cur;
		ss >> x;

		bytes.push_back((unsigned char)x);
	}
	return bytes;
}

Patch& Patch::operator<<( const std::string & str )
{
	std::vector<unsigned char> bytesToAdd = readString(str);
	bytes.insert(bytes.end(), bytesToAdd.begin(), bytesToAdd.end());
	return *this;
}

Patch::Patch(DWORD _address, bool _enabled, const std::string &_name)
	:address(_address)
	,enabled(_enabled)
	,applied(false),
	name(_name)
{
}

void Patch::apply()
{
	assert(!applied);
	applied = true;
	originalBytes.resize(bytes.size(),0);
	std::ostringstream os;
	os<<std::hex<<address<<std::endl;
	ReadProcessMemory(GetCurrentProcess(),(LPVOID)address,originalBytes.data(),bytes.size(),NULL);
	WriteProcessMemory(GetCurrentProcess(),(LPVOID)address,bytes.data(),bytes.size(),NULL);


	VirtualProtect((LPVOID)address,bytes.size(),PAGE_EXECUTE,&oldProtect);
}
void Patch::revert()
{
	assert(applied);
	applied = false;
	WriteProcessMemory(GetCurrentProcess(),(LPVOID)address,originalBytes.data(),originalBytes.size(),NULL);
	originalBytes.resize(0);
	VirtualProtect((LPVOID)address,bytes.size(),oldProtect,NULL);
}
