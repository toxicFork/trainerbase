#include "hook.h"
#include <detours.h>
#include <sstream>

Hook::Hook( const std::string &_procName, PVOID _hk, PVOID *_orig )
	:moduleName(""),
	funcName(_procName),
	hookFunc(_hk),
	origFunc(_orig),
	isInternal(false){}

Hook::Hook( DWORD address, PVOID _hk, PVOID *_orig )
	:moduleName(""),
	funcName(""),
	hookFunc(_hk),
	origFunc(_orig),
	isInternal(true){
		*origFunc = (PVOID)address;
}

Hook::Hook( const std::string &_moduleName, const std::string &_procName, PVOID _hk, PVOID *_orig )
	:moduleName(_moduleName),
	funcName(_procName),
	hookFunc(_hk),
	origFunc(_orig),
	isInternal(false){}

long Hook::applyHook()
{
	if(!isInternal)
		*origFunc = DetourFindFunction(moduleName.c_str(),funcName.c_str());

	LONG failed = 0;
	if(*origFunc==NULL)
	{
		failed = -1;
	}


	if(!failed)
	failed = DetourTransactionBegin();
	if(!failed)
	failed = DetourUpdateThread(GetCurrentThread());
	if(!failed)
	failed = DetourAttach(origFunc,hookFunc);
	if(!failed)
	failed = DetourTransactionCommit();

	//std::ostringstream o;
	//o<<moduleName.c_str()<<"\t"<<funcName.c_str()<<"\t"<<*origFunc<<" ..."<<failed;
	//MessageBox(NULL,o.str().c_str(),NULL,NULL);

	return failed;
}

void Hook::remove()
{
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(origFunc,hookFunc);
	DetourTransactionCommit();
	if(!isInternal)
		*origFunc = NULL;
}

void Hook::setModuleName( const std::string &param )
{
	moduleName = param;
}