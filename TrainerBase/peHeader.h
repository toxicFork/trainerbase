#pragma once

#include <Windows.h>
#include <string>

class PEHeader
{
public:
	static void getTextSectionInfo( LPSTR filePath, DWORD &imageBase, DWORD &start, DWORD &size );
};