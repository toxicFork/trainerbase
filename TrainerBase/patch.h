#pragma once
#include <vector>
#include <Windows.h>
#include <string>

class Patch
{
public:
	Patch(DWORD address, bool enabled, const std::string &name);
	Patch& operator<<(unsigned char c);
	template<size_t t> inline Patch& operator<<(const unsigned char (&c)[t])
	{
		Patch &p = *this;
		for(size_t i=0;i<t;++i)
		{
			p = p<<c[i];
		}
		return p;
	}
	Patch& operator<<(const std::string & str);
	bool isEnabled() const { return enabled; }
	void apply();
	void revert();
private:
	std::vector<unsigned char> bytes;
	std::vector<unsigned char> originalBytes;
	DWORD address;
	bool enabled;
	bool applied;
	std::string name;
	DWORD oldProtect;
};