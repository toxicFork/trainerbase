#pragma once

#include <vector>
#include <map>
#include <Windows.h>
#include "patch.h"
#include <sstream>

#define HOOKAPI extern "C" __declspec(dllexport)

namespace boost
{
	class thread;
}

#define LOG(msg)\
{\
	std::ostringstream os;\
	os<<msg;\
	logMessage(os.str().c_str());\
}

class InjectorThread;
class Hook;

typedef std::map<DWORD,InjectorThread*> processMap;

class Trainer
{
public:
	Trainer(std::string name, std::string exeName);
	~Trainer();

	const std::string& getName() const { return name; }
	const bool getMemcpy() const { return replaceMemcpy; }
	void processExited(DWORD pid);
	void start();
	HMODULE getModule();
	void justInjected();
	void justEjected();
protected:
	Patch &addPatch(DWORD address,bool enabled=false, const std::string &name="");
	void addHook(const std::string &moduleName, const std::string &procName, PVOID hk, PVOID *orig);
	void addHook( DWORD address, PVOID hk, PVOID *orig );
	bool replaceMemcpy;
private:
	std::string name, exeName;
	bool isInside;
	boost::thread *mainThread;
	bool running;
	processMap processes;
	std::vector<DWORD> deletionQueue;

	void insideProcess(DWORD gamePID);
	void run();
	std::vector<Hook*> hooks;
	Hook* memcpyHook;
	Hook* debugBPHook;

	std::vector<Patch> patches;
	void logMessage(const char* os);
};