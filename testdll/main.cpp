#include "main.h"
#include <Windows.h>
#include <iostream>
#include <sstream>

void WINAPI setup()
{
	test();

	HMODULE module = GetModuleHandle(NULL);
	std::ostringstream o;
	char name[MAX_PATH];
	GetModuleFileName(module,name,MAX_PATH);
	FARPROC fp = GetProcAddress(module,"init");

	if(!fp)
	{
		o<<GetLastError();
	}
	else
	{
		o<<fp;
	}
	o<<" "<<name<<std::endl;
	MessageBox(NULL,o.str().c_str(),NULL,NULL);
}

testdll_EXPORT void init()
{
	MessageBox(NULL,"init!",NULL,NULL);
}
