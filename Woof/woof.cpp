#include <trainerBase.h>
#include <cassert>

#define hook(retval,prefix,name,...)  retval ( prefix *orig_##name ) (__VA_ARGS__) = NULL;\
	retval hk_##name (__VA_ARGS__)

hook(int,WINAPI,MessageBoxA,HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
	assert(false);
	//return true;
	return orig_MessageBoxA(hWnd,lpText,lpCaption,uType);
}

hook(int,__thiscall,sub_A9E310,void *self, float a2)
{
	return 1;
}

class Woof : public Trainer
{
public:
	Woof():Trainer("Woof","HKShip.exe"){
		///*addPatch(0x0068D35A,true)<<"C390";
		//addPatch(0x0071D295,true)<<"EB";
		//addPatch(0x0099DA4B,true)<<"EB1D";
		//addPatch(0x0071D27A,true)<<"9090";
		//addPatch(0x00A7ACF1,true)<<"E9C300000090";
		//addPatch(0x00667CBD,true)<<"E99100000090";
		//addPatch(0x00667D46,true)<<"EB0B00000090";
		//addPatch(0x004283DD,true)<<"E95D03000090";
		//addPatch(0x0054D511,true)<<"E9D4000000909090909090";
		//addPatch(0x00606359,true)<<"EB20";
		//addPatch(0x00787F13,true)<<"E92902000090";
		//addPatch(0x004BB36E,true)<<"EB3E";o
		//addPatch(0x007CB588,true)<<"EB29";*/
		////addPatch(0x005A7C51,true)<<"EB33";
		//addPatch(0x0054ACE4,true)<<"c39090909090909090909090909090";
		//addPatch(0x00AC0826,true)<<"9090909090";
		//addPatch(0x00728DD7,true)<<"EB1F";
		//addPatch(0x00728E26,true)<<"EB";
		//addPatch(0x010B15A0,true)<<"c3";
		//addPatch(0x0086D9FE,true)<<"EB0D";
		//addPatch(0x007493B8,true)<<"c3";
		
		//addPatch(0x010C0FF0,true)<<"c3";
		// 
		//.TEXT CHECK? HKShip.AK::Monitor::PostString+140D3E - EB 0D                 - jmp HKShip.AK::Monitor::PostString+140D4D

		//addPatch(0x00A5CC82,true)<<"EB7C";
		//addPatch(0x0067E6C5,true)<<"E94C03000090";
		addPatch(0x005F8A70,true)<<"C3";


		
		////addPatch(0x00AC0AFE,true)<<"9090909090909090909090";
		//addPatch(0x00622B10,true)<<"C3";
		//addPatch(0x00A6533B,true)<<"9090909090909090909090909090909090909090909090909090909090";
		//addPatch(0x0040B8F4B,true)<<"9090";
		//



		////HKShip.AK::Monitor::PostString+393B66 - 90                    - nop 
		////HKShip.AK::Monitor::PostString+393B67 - 90                    - nop 
		////HKShip.AK::Monitor::PostString+393B68 - 90                    - nop 
		////HKShip.AK::Monitor::PostString+393B69 - 90                    - nop 
		////HKShip.AK::Monitor::PostString+393B6A - 90                    - nop 




		addHook("user32.dll","MessageBoxA",hk_MessageBoxA,&(PVOID&)orig_MessageBoxA);
		//addHook(0x00A9E310,hk_sub_A9E310,&(PVOID&)orig_sub_A9E310);
		//addHook("ntdll.dll","DbgBreakPoint")

		


	}
private:
	~Woof();
};

Woof *singleton = new Woof();