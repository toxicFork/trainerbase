#include <trainerBase.h>


class JC2 : public Trainer
{
public:
	JC2():Trainer("BOLOPatch","JustCause2.exe"){
		replaceMemcpy = true;

		const unsigned char buf[] = {0x90,0x90};
		//addPatch(0x00421C0F,true)<<buf;
		//addPatch(0x00421C13,true)<<buf;

		addPatch(0x00D91E00,true)<<0xc3; //ret
		addPatch(0x00D91D40,true)<<0xc3;
		addPatch(0x00D91E60,true)<<0xc3;
	}
private:
	~JC2();
};

JC2 *singleton = new JC2();