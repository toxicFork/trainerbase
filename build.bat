@echo off
set OLDDIR=%CD%
IF EXIST ./build GOTO NEXT
mkdir build
:NEXT
cd ./build
cmake -g "Visual Studio 10" "../"
chdir /d %OLDDIR% &rem restore current directory
pause
echo on