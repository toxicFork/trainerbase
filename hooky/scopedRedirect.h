#pragma once

#include <iostream>
#include <fstream>


class ScopedRedirect
{
public:
    ScopedRedirect(std::ostream & inOriginal, std::streambuf *inRedirect) :
        mOriginal(inOriginal)
    {
       former_buff =  mOriginal.rdbuf(inRedirect);
    }

    ~ScopedRedirect()
    {
        mOriginal.rdbuf(former_buff);
    }    

private:
    ScopedRedirect(const ScopedRedirect&);
    ScopedRedirect& operator=(const ScopedRedirect&);

    std::ostream & mOriginal;
	std::streambuf *former_buff;
};