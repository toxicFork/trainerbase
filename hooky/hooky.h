#pragma once

#include <string>
#include <Hooky_Export.h>
#include <sstream>

class Hook;
class Hooky_EXPORT Hooky
{
public:
	Hooky(const std::string exeName, std::streambuf *newOutputStream=NULL);
	~Hooky();

	void redirectOutput(std::streambuf *newOutputStream);
};

