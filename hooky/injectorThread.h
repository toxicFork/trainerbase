#pragma once

#include <Windows.h>
#include <boost/thread.hpp>


class Hook;

class InjectorThread
{
private:
	Hook* parent;
public:
	InjectorThread(Hook* parent, HMODULE dllModule, DWORD pid);
	~InjectorThread();
private:
	void run();
	void eject();
	boost::thread *thread;
	bool dead, suicide;
	HMODULE dllModule;
	DWORD pid;
};

namespace boost
{
	template<class T> class shared_ptr;
}
typedef  boost::shared_ptr<InjectorThread> PInjectorThread;
