#include "injectorThread.h"
#include <string>
#include "hook.h"

InjectorThread::InjectorThread(Hook* _parent, HMODULE _dllModule, DWORD _pid)
	:parent(_parent),
	dllModule(_dllModule),
	pid(_pid),
	dead(false),
	suicide(false)
{
	thread = new boost::thread(&InjectorThread::run,this);
}

void InjectorThread::run()
{
	HANDLE hProc;
	hProc = OpenProcess(PROCESS_ALL_ACCESS, 0, pid);

	if(!hProc)
	{
		return;
	}

	TCHAR szFileName[MAX_PATH];
	GetModuleFileName(dllModule, szFileName, MAX_PATH);
	std::string dllPath(szFileName);
	int len = dllPath.length() + 2;
	void* pRemoteString = VirtualAllocEx(hProc, 0, len * sizeof(char), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	WriteProcessMemory(hProc, pRemoteString, (void*)dllPath.c_str(), len * sizeof(char), 0);
	FARPROC pLoadLibrary = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	HANDLE hThread = CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)pLoadLibrary, pRemoteString, 0, 0);

	if(!hThread)
		return;

	if(hThread)
	{
		WaitForSingleObject(hThread,INFINITE);
		GetExitCodeThread(hThread, (DWORD*)&dllModule);

		std::ostringstream o;
		o<<"Injected into game process: "<<pid<<"!";
		parent->trace(o.str());
	}
	VirtualFreeEx(hProc, pRemoteString, len * sizeof(wchar_t), MEM_FREE);

	while(true)
	{
		if(suicide)
			break;

		if(dead)
		{
			std::ostringstream o;
			o<<"Ejecting from process: "<<pid<<"!";
			parent->trace(o.str());
			eject();
			break;
		}
		if(WaitForSingleObject(hProc,0)==WAIT_OBJECT_0)
		{
			suicide = true;
			parent->processExited(pid);
			//MessageBox(NULL,"I GUESS IT'S OVER NOW AIN'T IT",NULL,NULL);
			break;
		}
		Sleep(20);
	}
	
	CloseHandle(hProc);
}

void InjectorThread::eject()
{
	HANDLE hProc = OpenProcess((PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION |
		PROCESS_QUERY_LIMITED_INFORMATION | PROCESS_VM_OPERATION |
		PROCESS_VM_WRITE | PROCESS_VM_READ), 0, pid);
	HANDLE hThread = CreateRemoteThread(hProc, NULL, 0, (LPTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle("kernel32.dll"),"FreeLibrary"), (void*)dllModule, 0, NULL);

	std::ostringstream o;
	if(hThread)
	{
		WaitForSingleObject(hThread,INFINITE);
		o<<"Ejected successfully from game process: "<<pid<<"!";
	}
	else{
		o<<"Could not create remote ejector thread for process: "<<pid<<"!";
	}
	parent->trace(o.str());
	CloseHandle(hProc);
}


InjectorThread::~InjectorThread()
{
	dead = true;
	if(thread){
		thread->join();
		delete thread;
		thread = NULL;
	}
}