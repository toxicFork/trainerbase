#include "hooky.h"
#include "hook.h"

#include <boost/interprocess/managed_windows_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <vector>

boost::interprocess::managed_windows_shared_memory shdmem(boost::interprocess::open_or_create,"HOOKY",1024*1024);
typedef boost::interprocess::allocator<int, boost::interprocess::managed_windows_shared_memory::segment_manager> ShmemAllocator;

//Alias a vector that uses the previous STL-like allocator
//typedef std::vector<int, ShmemAllocator> MyVector;
//const ShmemAllocator alloc_inst (shdmem.get_segment_manager());
//MyVector *myvector = shdmem.construct<MyVector>("MyVector") //object name
//	(alloc_inst);//first ctor parameter

Hooky::Hooky(const std::string exeName, std::streambuf *newOutputStream)
{
	if(newOutputStream)
		Hook::imp->redirectOutput(newOutputStream);
	Hook::imp->begin(exeName);
}

Hooky::~Hooky()
{
	Hook::imp->end();
}

void Hooky::redirectOutput( std::streambuf *newOutputStream )
{
	Hook::imp->redirectOutput(newOutputStream);
}

//
bool hooked = false;
int derp = 0;

#include <windows.h>
BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpvReserved)
{
	//region = boost::interprocess::mapped_region(shdmem, boost::interprocess::read_write); 

	std::ostringstream o;
//	dllModule = hModule;
	switch(dwReason){
	case DLL_PROCESS_ATTACH:
//		//std::cout<<"attached!\n";
//		//TCHAR szFileName[MAX_PATH];
//		//GetModuleFileName(NULL, szFileName, MAX_PATH);
		//		//MessageBoxA(NULL,szFileName,"Hooky",NULL);
		//imp = new Hook(_exeName,_filePath);
		//imp->start(hModule);
		if(!lpvReserved)
		{
			hooked = true;
			Hook::imp = shdmem.find<Hook>("HOOK").first;
			Hook::imp->injectedStart();
//			os<<"HERP IS "<<imp->herp;
			//MessageBox(NULL,os.str().c_str(),NULL,NULL);
			//try{
			//}
			//catch(std::exception &e)
			//{
				//MessageBox(NULL,e.what(),NULL,NULL);
			//}
			//MessageBox(NULL,os.str().c_str(),NULL,NULL);
			//anchor->increment();
//			trace("------------");
//			trace("Hooked!");
//			traceIndent();
//
//			byte buf[] = {0x90,0x90};
//			patchMemory(0x00421C0F,buf);
//			patchMemory(0x00421C13,buf);
//
//			patchMemory(0x00D91E00,ASM_RET); //ret
//			patchMemory(0x00D91D40,ASM_RET);
//			patchMemory(0x00D91E60,ASM_RET);
//
//			DetourTransactionBegin();
//			DetourUpdateThread(GetCurrentThread());
//			apply_hook(msvcr80.dll,memcpy);
//			//apply_hook(d3dx10_42.dll,D3DX10CreateDevice);
//			//apply_hook(kernel32.dll, LoadLibraryA);
//			DetourTransactionCommit();
//
//			open_file_mapping();
//			trace("File mapping complete!!");
//			
//			//MessageBoxA(NULL,"Hooked!","Hooky",NULL);
		}
		else
		{	
			derp = 20;
			Hook::imp = shdmem.construct<Hook>("HOOK")();
			//imp = new Hook();
			Hook::imp->dllStart(hModule);
//			imp->herp = -6;
			Hook::imp = shdmem.find<Hook>("HOOK").first;
//			std::cout<<imp->herp<<std::endl;
		}
		o<<derp<<std::endl;
		MessageBox(NULL,o.str().c_str(),NULL,NULL);
		break;
	case DLL_PROCESS_DETACH:
		if(hooked)
		{
			//imp = shdmem.find<Hook>("HOOK").first;
			//imp->trace("Exited!");
			//MessageBox(NULL,"FINALLY!",NULL,NULL);
//			trace("Unhooked!");
		}
		else{
			shdmem.destroy_ptr(Hook::imp);
		}
		break;
	}
//
	return TRUE;
}
//#include "anchor.h"
//#include <iostream>
//#include <Psapi.h>
//#include <array>
//#include <exception>
//
//#include <signal.h>
//
//#include <D3DX10.h>
//
//#include "hook.h"
//#include <detours.h>
//
//#include <stdarg.h>
//
//#include <sstream>
//
//
//HANDLE fileHandle, mappingHandle;
//PVOID viewOfFile;
//
//#define hook(retval,prefix,name,...)  retval ( prefix *orig_##name ) (__VA_ARGS__) = NULL;\
//	static retval hk_##name (__VA_ARGS__)
//
//#define apply_hook(dl,h) *(&(PVOID&)orig_##h) = DetourFindFunction(#dl,#h);\
//	DetourAttach(&(PVOID&)orig_##h,&hk_##h)
//
//unsigned int traceIndentation=0;
//void traceIndent()
//{
//	traceIndentation++;
//}
//
//void trace(std::string message)
//{
//	for(unsigned int i=0;i<traceIndentation;i++)
//		message = "\t"+message;
//	message+="\n";
//	FILE* f;
//	fopen_s(&f,"Hooky.log","a");
//	fprintf(f,message.c_str());
//	fclose(f);
//}
//
//hook(void*,__cdecl,memcpy,void* dst,const void* src,size_t size)
//{
//	void* ret = nullptr;
//	// .text section base address
//	static const DWORD text_start = 0x401000;
//	// .text section end address
//	static const DWORD text_end = text_start + 0x992000; // 
//	//TODO find text section dynamically
//	// Offset into file where .text section starts
//	static const DWORD file_text_start = 0x400;
//
//	DWORD src_addr = reinterpret_cast<DWORD>(src);
//	BYTE* view_of_file = static_cast<BYTE*>(
//		static_cast<PVOID>(viewOfFile));
//
//	// If the source address is within the .text section, return data
//	// read from the file mapping instead, so that the hashing routine
//	// does not see any modifications.
//	if ( src_addr >= text_start && src_addr <= text_end )
//	{
//		ret = orig_memcpy(dst, 
//			view_of_file + file_text_start + (src_addr - text_start), size);
//	} else {
//		ret = orig_memcpy(dst, src, size);
//	}
//
//	return ret;
//}
//
//
//void open_file_mapping()
//{
//	//std::array<wchar_t, MAX_PATH> file_name;
//	//GetModuleFileNameW( nullptr, &file_name[0], file_name.size() );
//	TCHAR szFileName[MAX_PATH];
//	GetModuleFileName(NULL, szFileName, MAX_PATH);
//	//MessageBoxA(NULL,szFileName,"a",NULL);
//
//	fileHandle = CreateFileA( szFileName, GENERIC_READ, FILE_SHARE_READ,
//		nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr );
//
//	if ( !fileHandle )
//		throw std::runtime_error( "Failed to open game exe!" );
//
//	DWORD file_size(0);
//	GetFileSize( fileHandle, &file_size );
//
//	mappingHandle = CreateFileMapping( fileHandle, nullptr, 
//		PAGE_READONLY, file_size, 0, nullptr );
//
//	//MessageBoxA(NULL,"b","",NULL);
//	if ( !mappingHandle )
//		throw std::runtime_error( "Failed to create file mapping" );
//	//MessageBoxA(NULL,"c","",NULL);
//
//	viewOfFile = MapViewOfFile( mappingHandle, FILE_MAP_READ, 0, 0, 0 );
//	if ( !viewOfFile )
//		throw std::runtime_error( "Failed to map view of file" );
//	
//}
//
////IDXGIAdapter *adapter;
////hook(HRESULT,WINAPI,D3DX10CreateDevice,
////		IDXGIAdapter *pAdapter,
////		D3D10_DRIVER_TYPE DriverType,
////		HMODULE Software,
////		UINT Flags,
////		ID3D10Device **ppDevice)
////{
////	__asm int 3
////	adapter = pAdapter;
////	return orig_D3DX10CreateDevice(pAdapter,DriverType,Software,Flags,ppDevice);
////}
//
//hook(HMODULE,__stdcall,LoadLibraryA,LPCSTR lpLibFileName)
//{
//	trace("loading lib: "+std::string(lpLibFileName));
//	return orig_LoadLibraryA(lpLibFileName);
//}
//
//void ProcessDebugEvent(DEBUG_EVENT* evt)
//{
//	char message[MAX_PATH];
//	sprintf_s(message,"%d %d %d",evt->dwDebugEventCode,evt->dwProcessId,evt->dwThreadId);
//	trace(message);
//}
//
//inline void patchMemory(DWORD address, byte toPatch){
//	SIZE_T wrt;
//	WriteProcessMemory(GetCurrentProcess(),(LPVOID)address,&toPatch,sizeof(byte),&wrt);
//}
//
//template<size_t n> inline void patchMemory(DWORD address, byte (&toPatch)[n]){
//	SIZE_T wrt;
//	WriteProcessMemory(GetCurrentProcess(),(LPVOID)address,&toPatch,sizeof(byte)*n,&wrt);
//}
//#define ASM_RET (0xC3)
//
//
//HMODULE dllModule;
//BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpvReserved)
//{
////	dllModule = hModule;
//	switch(dwReason){
//	case DLL_PROCESS_ATTACH:
////		//std::cout<<"attached!\n";
////		//TCHAR szFileName[MAX_PATH];
////		//GetModuleFileName(NULL, szFileName, MAX_PATH);
////		//MessageBoxA(NULL,szFileName,"Hooky",NULL);
//		if(!l3pvReserved)
//		{
//			//anchor->increment();
////			trace("------------");
////			trace("Hooked!");
////			traceIndent();
//			hooked = true;
////
////			byte buf[] = {0x90,0x90};
////			patchMemory(0x00421C0F,buf);
////			patchMemory(0x00421C13,buf);
////
////			patchMemory(0x00D91E00,ASM_RET); //ret
////			patchMemory(0x00D91D40,ASM_RET);
////			patchMemory(0x00D91E60,ASM_RET);
////
////			DetourTransactionBegin();
////			DetourUpdateThread(GetCurrentThread());
////			apply_hook(msvcr80.dll,memcpy);
////			//apply_hook(d3dx10_42.dll,D3DX10CreateDevice);
////			//apply_hook(kernel32.dll, LoadLibraryA);
////			DetourTransactionCommit();
////
////			open_file_mapping();
////			trace("File mapping complete!!");
////			
////			//MessageBoxA(NULL,"Hooked!","Hooky",NULL);
//		}
//		else
//		{
//			//anchor = new Anchor(hModule);
//		}
////		//std::cout<<"attached!!!\n";
//		break;
//	case DLL_PROCESS_DETACH:
//		if(hooked)
//		{
////			trace("Unhooked!");
//		}
//		else
//		{
//			//delete anchor;
////
//		}
//		break;
////	}
////
//	return TRUE;
//}
//
//DWORD findProcess(std::string pName)
//{
//	unsigned long aProcesses[1024], cbNeeded, cProcesses;
//	if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
//		return -1;
//
//	cProcesses = cbNeeded / sizeof(unsigned long);
//	for(unsigned int i = 0; i < cProcesses; i++)
//	{
//		if(aProcesses[i] == 0)
//			continue;
//
//		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, aProcesses[i]);
//		char buffer[50];
//		GetModuleBaseName(hProcess, 0, buffer, 50);
//		CloseHandle(hProcess);
//		if(pName == std::string(buffer))
//			return aProcesses[i];
//	}
//	return -1;
//}
//
//Hooky_EXPORT void initHook( std::string exeName, std::string filePath /*= ""*/ )
//{
//	while(true)
//	{
//		DWORD pid = -1;
//		HANDLE hProc;
//
//		if(exeName.length()>0)
//		{
//			trace("waiting for process!");
//			while(pid==-1)
//			{
//				pid = findProcess(exeName);
//				Sleep(20);
//			}
//		}
//
//		hProc = OpenProcess((PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION |
//			PROCESS_QUERY_LIMITED_INFORMATION | PROCESS_VM_OPERATION |
//			PROCESS_VM_WRITE | PROCESS_VM_READ), 0, pid);
//
//		if(!hProc)
//		{
//			return;
//		}
//
//		TCHAR szFileName[MAX_PATH];
//		GetModuleFileName(dllModule, szFileName, MAX_PATH);
//		std::string dllPath(szFileName);
//		int len = dllPath.length() + 2;
//		void* pRemoteString = VirtualAllocEx(hProc, 0, len * sizeof(char), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
//		WriteProcessMemory(hProc, pRemoteString, (void*)dllPath.c_str(), len * sizeof(char), 0);
//		FARPROC pLoadLibrary = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
//		HANDLE hThread = CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)pLoadLibrary, pRemoteString, 0, 0);
//		if(hThread)
//		{
//			WaitForSingleObject(hThread,INFINITE);
//		}
//		VirtualFreeEx(hProc, pRemoteString, len * sizeof(wchar_t), MEM_FREE);
//
//		CloseHandle(hProc);
//		if(!hThread)
//			return;
//
//		trace("done!");
//
//		while(findProcess(exeName)==pid)
//			Sleep(20);
//	}
//}