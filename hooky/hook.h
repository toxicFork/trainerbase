#pragma once

#include <string>
#include <Windows.h>
#include <map>
#include <vector>
#include <boost/thread.hpp>
#include <sstream>
#include "scopedRedirect.h"


class InjectorThread;
namespace boost
{
	template<class T> class shared_ptr;
}

typedef  boost::shared_ptr<InjectorThread> PInjectorThread;

class Hook
{
public:
	Hook();
	~Hook();

	void dllStart( HMODULE module );
	void terminate();
	void begin(const std::string &exeName);
	void hooked( HMODULE hModule );
	void redirectOutput(std::streambuf *newOutputStream);
	void processExited(DWORD pid);

	void trace(std::string message);
	void injectedStart();
	void end();

	static Hook* imp;
private:
	std::string exeName;
	HMODULE dllModule;

	void run();
	static DWORD WINAPI traceFromOutside(LPVOID param);
	std::vector<DWORD> findUnhookedProcesses( );
	void flushStreams();
	std::map<DWORD,PInjectorThread> injectors;
	boost::thread *thread;
	bool running,dead;

	std::ostringstream hookStream, queueStream;
	std::vector<std::string> streamQueue;

	ScopedRedirect *outputRedirect;

	DWORD mainProcess;
};