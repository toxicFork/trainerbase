#include "hook.h"
#include <Psapi.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include "injectorThread.h"

Hook* Hook::imp = NULL;

Hook::Hook()
	:exeName(MAX_PATH,' ')
{
	mainProcess = GetCurrentProcessId();
	outputRedirect = new ScopedRedirect(hookStream,std::cout.rdbuf());
}

void Hook::begin( const std::string &_exeName)
{
	dead = false;
	running = false;

	hookStream<<"Starting hook!"<<std::endl;
	exeName = _exeName;

	thread = new boost::thread(&Hook::run,this);
}

void Hook::end()
{
	trace("Cleaning up...");
	injectors.clear();

	dead = true;
	if(thread)
	{
		if(running)
			thread->join();
		delete thread;
		thread = NULL;
	}


	trace("Cleanup complete!");
	flushStreams();

	if(outputRedirect)
		delete outputRedirect;

	outputRedirect = new ScopedRedirect(hookStream,std::cout.rdbuf());
}

Hook::~Hook()
{
	end();
}

std::vector<DWORD> Hook::findUnhookedProcesses()
{
	std::vector<DWORD> processes;
	unsigned long aProcesses[1024], cbNeeded, cProcesses;
	if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return processes;

	cProcesses = cbNeeded / sizeof(unsigned long);
	for(unsigned int i = 0; i < cProcesses; i++)
	{
		if(aProcesses[i] == 0||injectors.find(aProcesses[i])!=injectors.end())
			continue;

		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, aProcesses[i]);
		char buffer[MAX_PATH];
		GetModuleBaseName(hProcess, 0, buffer, MAX_PATH);
		CloseHandle(hProcess);
		if(exeName == std::string(buffer))
			processes.push_back(aProcesses[i]);
	}
	return processes;
}

void Hook::run()
{
	dead = false;
	running = true;
	trace("Initializing...");
	queueStream<<"Looking for processes named "<<exeName<<"..."<<std::endl;
	while(!dead)
	{
		std::vector<DWORD> processes = findUnhookedProcesses();
		if(!processes.empty())
		{
			queueStream<<"Found "<<processes.size()<<" unhooked process"<<(processes.size()>1?"es":"")<<"."<<std::endl;

			for(unsigned i=0;i<processes.size();i++)
			{
				injectors[processes[i]] = PInjectorThread(new InjectorThread(this,dllModule,processes[i]));
			}
		}

		flushStreams();
		Sleep(100);
	}
	running = false;
}



DWORD WINAPI Hook::traceFromOutside(LPVOID param)
{
	std::ostringstream os;

	DWORD *pid = (DWORD*)param;
	os<<"("<<*pid<<"): "<<(LPCSTR)param+sizeof(DWORD)/sizeof(char);
	imp->trace(os.str());
	return NULL;
}

void Hook::trace( std::string message )
{
	if(GetCurrentProcessId()!=mainProcess){
		HANDLE hProc = OpenProcess((PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION |
					PROCESS_QUERY_LIMITED_INFORMATION | PROCESS_VM_OPERATION |
					PROCESS_VM_WRITE | PROCESS_VM_READ), 0, mainProcess);

		size_t lenMessage = (message.length()+2)*sizeof(char);
		size_t messageOffset = sizeof(DWORD);
		size_t totalLen = messageOffset+lenMessage;

		//int len = message.length() + 2;
		void* pRemoteString = VirtualAllocEx(hProc, 0, totalLen, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

		DWORD pid = GetCurrentProcessId();
		WriteProcessMemory(hProc, pRemoteString, (void*)&pid, sizeof(DWORD), 0);
		WriteProcessMemory(hProc, (char*)pRemoteString+messageOffset, (void*)message.c_str(), lenMessage, 0);

		HANDLE hThread = CreateRemoteThread(hProc, NULL, 0, &traceFromOutside, pRemoteString, 0, NULL);
		if(hThread)
		{
			WaitForSingleObject(hThread,INFINITE);
		}

		VirtualFreeEx(hProc, pRemoteString, lenMessage, MEM_FREE);
		CloseHandle(hProc);
	}
	else{
		streamQueue.push_back(message);
		//hookStream<<message.c_str()<<std::endl;
	}
}

void Hook::dllStart( HMODULE _dllModule )
{
	dllModule = _dllModule;
}

void Hook::terminate()
{
	trace("finitto!");
}

void Hook::hooked( HMODULE hModule )
{
	std::ostringstream os;
	os<<exeName<<" "<<hModule<<std::endl;
	trace(os.str());
}

void Hook::redirectOutput( std::streambuf *newOutputStream )
{
	if(outputRedirect)
		delete outputRedirect;
	outputRedirect = new ScopedRedirect(hookStream,newOutputStream);
}

void Hook::processExited( DWORD pid )
{
	queueStream<<"Process exited: "<<pid<<std::endl;
	injectors.erase(injectors.find(pid));
}

#include "peHeader.h"

void Hook::injectedStart()
{
	TCHAR szFileName[MAX_PATH];
	GetModuleFileName(NULL, szFileName, MAX_PATH);
	trace("Initializing...");

	DWORD base, start, size;
	PEHeader::getTextSectionInfo(szFileName,&base,&start,&size);
	std::ostringstream os;
	os<<".text section start: 0x"<<std::hex<<base+start;
	trace(os.str());
	os.str("");
	os<<".text section finish: 0x"<<std::hex<<+base+start+size;
	trace(os.str());
}

void Hook::flushStreams()
{
	if(!queueStream.tellp())
	{
		hookStream<<queueStream.str().c_str();
		queueStream.clear();
		queueStream.str(std::string());
	}

	if(!streamQueue.empty())
	{
		for(unsigned int i=0;i<streamQueue.size();i++)
		{
			hookStream<<streamQueue[i].c_str()<<std::endl;
		}
		streamQueue.clear();
	}
}

